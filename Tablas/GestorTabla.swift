//
//  GestorTabla.swift
//  Tablas
//
//  Created by Jesús Gallego Irles on 28/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import Foundation
import UIKit

class GestorTabla: NSObject, UITableViewDataSource, UITableViewDelegate {
    var nombres = ["Daenerys Targaryen", "Jon Nieve", "Cersei Lannister", "Eddard Stark"]
    
    static public var LAST = 1
    static public var FIRST = 2
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nombres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = nombres[indexPath.row]
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            nombres.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
    
    public func insertarCelda(enTabla tabla: UITableView, enFila: Int, conTexto texto: String) {
        var index : Int
        
        switch enFila {
        case GestorTabla.LAST:
            index = nombres.count
        case GestorTabla.FIRST:
            index = 0
        default:
            index = nombres.count
        }
        
        nombres.insert(texto, at: index)
        tabla.reloadData()
    }
    
}
