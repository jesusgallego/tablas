//
//  ViewController.swift
//  Tablas
//
//  Created by Master Móviles on 11/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ListaViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var miDS : GestorTabla?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.miDS = GestorTabla()
        tableView.dataSource = miDS
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onInsertarClicked(_ sender: AnyObject) {
        if let text = textField.text {
            if text != "" {
                miDS?.insertarCelda(enTabla: tableView, enFila: GestorTabla.LAST, conTexto: text)
                textField.text = ""
            }
        }
    }
    
    @IBAction func onEdicionClicked(_ sender: AnyObject) {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
}

